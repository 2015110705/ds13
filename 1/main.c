/*
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define MAX_STACK_SIZE 100

/* precedence */
typedef enum {	lparen, rparen, plus, minus, 
				times, divide, mod, eos, operand} precedence;

/* tree structure */
typedef struct node *treePointer;
typedef struct node {
	char		data;		//문자 출력을 위해 char 형으로 지정
	treePointer leftChild;
	treePointer rightChild;
}tNode;
treePointer root;

/* stack structure */
treePointer stack[MAX_STACK_SIZE];
int			top = -1;
char		expr[81];		/* postfix expression */

/* traversal function */
void		inorder(treePointer ptr);
void		preorder(treePointer ptr);
void		postorder(treePointer ptr);

/* binary tree function */
void		createBinTree();

/* stack function */
void		push(treePointer ptr);
treePointer pop();
void		stackFull();
treePointer	stackEmpty();

/* get token function */
precedence	getToken(char *symbol, int *n);

int main()
{
	FILE *pInputFile = fopen("input.txt", "r");
	
	/* data input */
	printf("the length of input string should be less than 80 \n");
	printf("input string <postfix expression> : ");
	fscanf(pInputFile, "%s", expr);
	printf("%s \n", expr);

	/* create binary tree */
	printf("creating its binary tree \n\n");
	createBinTree();

	/* print */
	/* inorder traversal */
	printf("inorder traversal\t\t: ");
	inorder(root);
	printf("\n");

	/* preorder traversal */
	printf("preorder traversal\t\t: ");
	preorder(root);
	printf("\n");

	/* postorder traversal */
	printf("postorder traversal\t\t: ");
	postorder(root);
	printf("\n");

	return 0;
}

/**
 * inorder traversal of a binary tree
 * @param	: ptr of tree's root
 */
void inorder(treePointer ptr)
{
	if (ptr) {
		inorder(ptr->leftChild);
		printf("%c", ptr->data);
		inorder(ptr->rightChild);
	}
}

/**
* preorder traversal of a binary tree
* @param	: ptr of tree's root
*/
void preorder(treePointer ptr)
{
	if (ptr) {
		printf("%c", ptr->data);
		preorder(ptr->leftChild);
		preorder(ptr->rightChild);
	}
}

/**
* postorder traversal of a binary tree
* @param	: ptr of tree's root
*/
void postorder(treePointer ptr)
{
	if (ptr) {
		postorder(ptr->leftChild);
		postorder(ptr->rightChild);
		printf("%c", ptr->data);
	}
}

/**
 * create binary tree with using linked list
 */
void createBinTree()
{
	/* scanning the expr from left to right */
	precedence	token;
	char		symbol;
	int			n = 0;

	token = getToken(&symbol, &n);
	while (token != eos) {
		/* if token is operand */
		if (token == operand) {
			/* make new node (*new node's both links are NULL)*/
			treePointer newNode = (treePointer)malloc(sizeof(tNode));
			newNode->leftChild = NULL;
			newNode->rightChild = NULL;
			newNode->data = symbol;

			/* push to stack */
			push(newNode);
		}
		/* if token is operator */
		else {
			/* make new node */
			treePointer newNode = (treePointer)malloc(sizeof(tNode));

			/* right child - pop node */
			newNode->rightChild = pop();

			/* left child - pop node */
			newNode->leftChild = pop();

			/* push operator node to stack */
			newNode->data = symbol;

			push(newNode);
		}

		/* root is node that lasts in stack */
		root = stack[top];
		token = getToken(&symbol, &n);
	}
}

/* stack function */
/**
 * stack push function
 * @param	: ptr in order to push to stack
 */
void push(treePointer ptr)
{
	if (top >= MAX_STACK_SIZE - 1) {
		stackFull();
	}
	stack[++top] = ptr;
}

/**
 * stack pop function
 * @return	: pop element
 */
treePointer pop()
{
	if (top == -1) {
		return stackEmpty();
	}

	return stack[top--];
}

/**
 * when push element if stack is full
 * print error and exit program
 */
void stackFull()
{
	fprintf(stderr, "Stack is full, cannot add element \n");
	exit(EXIT_FAILURE);
}

/**
 * when pop element if stack is empty
 * print error and exit program
 * @return	: NULL
 */
treePointer stackEmpty()
{
	fprintf(stderr, "Stack is empty, cannot delete element \n");
	exit(EXIT_FAILURE);
	return NULL;
}


/**
 * get the next token, symbol is the character
 * representation, which is returned, the token is
 * represented by its enumerated value, which
 * is returned in the function name
 */
precedence getToken(char *symbol, int *n)
{
	*symbol = expr[(*n)++];

	switch (*symbol) {
	case '('	: return lparen;
	case ')'	: return rparen;
	case '+'	: return plus;
	case '-'	: return minus;
	case '/'	: return divide;
	case '*'	: return times;
	case '%'	: return mod;
	case '\0'	: return eos;
	default		: return operand;	/*	no error checking,
										default is operand */
	}
}