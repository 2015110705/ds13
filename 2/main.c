/*
* 2015110705 김민규
* 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define MAX_STACK_SIZE 100
#define MAX_QUEUE_SIZE 100

/* precedence */
typedef enum {
	lparen, rparen, plus, minus,
	times, divide, mod, eos, operand
} precedence;

/* tree structure */
typedef struct node *treePointer;
typedef struct node {
	char		data;		//문자 출력을 위해 char 형으로 지정
	treePointer leftChild;
	treePointer rightChild;
}tNode;
treePointer root;

/* stack structure */
treePointer stack[MAX_STACK_SIZE];
int			top = -1;
char		expr[81];		/* postfix expression */

/* circular queue structure */
treePointer queue[MAX_QUEUE_SIZE];
int			front = 0;
int			rear = 0;

/* traversal function */
void		iterInorder(treePointer ptr);
void		levelOrder(treePointer ptr);

/* binary tree function */
void		createBinTree();

/* stack function */
void		push(treePointer ptr);
treePointer pop();
void		stackFull();
treePointer	stackEmpty();

/* queue function */
void		addq(treePointer item);
treePointer deleteq();
void		queueFull();
treePointer queueEmpty();

/* get token function */
precedence	getToken(char *symbol, int *n);

int main()
{
	FILE *pInputFile = fopen("input.txt", "r");

	/* data input */
	printf("the length of input string should be less than 80 \n");
	printf("input string <postfix expression> : ");
	fscanf(pInputFile, "%s", expr);
	printf("%s \n", expr);

	/* create binary tree */
	printf("creating its binary tree \n\n");
	createBinTree();

	/* print */
	/* iterative inorder traversal */
	printf("iterative inorder traversal\t: ");
	iterInorder(root);
	printf("\n");

	/* level order traversal */
	printf("level order traversal\t\t: ");
	levelOrder(root);
	printf("\n");

	return 0;
}

/**
 * iterative inorder traversal
 * @param	: node is tree's root
 */
void iterInorder(treePointer ptr)
{
	top = -1;
	while (1) {
		for (; ptr; ptr = ptr->leftChild) {
			push(ptr);	/* add to stack */
		}
		ptr = pop();	/* delete from stack */
		if (!ptr) 
			break;	/* empty stack */
		printf("%c", ptr->data);
		ptr = ptr->rightChild;
	}
}

/**
 * Level-order traversal of a binary tree
 * @param	: ptr is tree's root
 */
void levelOrder(treePointer ptr)
{
	if (!ptr)	
		return;	/* empty tree */
	addq(ptr);

	while (1) {
		ptr = deleteq();
		if (ptr) {
			printf("%c", ptr->data);
			if (ptr->leftChild) {
				addq(ptr->leftChild);
			}
			if (ptr->rightChild) {
				addq(ptr->rightChild);
			}
		}
		else {
			break;
		}
	}
}

/**
* create binary tree with using linked list
*/
void createBinTree()
{
	/* scanning the expr from left to right */
	precedence	token;
	char		symbol;
	int			n = 0;

	token = getToken(&symbol, &n);
	while (token != eos) {
		/* if token is operand */
		if (token == operand) {
			/* make new node (*new node's both links are NULL)*/
			treePointer newNode = (treePointer)malloc(sizeof(tNode));
			newNode->leftChild = NULL;
			newNode->rightChild = NULL;
			newNode->data = symbol;

			/* push to stack */
			push(newNode);
		}
		/* if token is operator */
		else {
			/* make new node */
			treePointer newNode = (treePointer)malloc(sizeof(tNode));

			/* right child - pop node */
			newNode->rightChild = pop();

			/* left child - pop node */
			newNode->leftChild = pop();

			/* push operator node to stack */
			newNode->data = symbol;

			push(newNode);
		}

		/* root is node that lasts in stack */
		root = stack[top];
		token = getToken(&symbol, &n);
	}
}

/* stack function */
/**
* stack push function
* @param	: ptr in order to push to stack
*/
void push(treePointer ptr)
{
	if (top >= MAX_STACK_SIZE - 1) {
		stackFull();
	}
	stack[++top] = ptr;
}

/**
* stack pop function
* @return	: pop element
*/
treePointer pop()
{
	if (top == -1) {
		return NULL;
	}

	return stack[top--];
}

/**
* when push element if stack is full
* print error and exit program
*/
void stackFull()
{
	fprintf(stderr, "Stack is full, cannot add element \n");
	exit(EXIT_FAILURE);
}

/**
* when pop element if stack is empty
* print error and exit program
* @return	: NULL
*/
treePointer stackEmpty()
{
	fprintf(stderr, "Stack is empty, cannot delete element \n");
	exit(EXIT_FAILURE);
	return NULL;
}

/**
 * add an item to the queue
 * @param	: item is add to queue
 */
void addq(treePointer item)
{
	rear = (rear + 1) % MAX_QUEUE_SIZE;

	if (front == rear) {
		queueFull();	/* print error and exit */
	}
	queue[rear] = item;
}

/**
 * remove front element from the queue
 * @return	: remove element
 */
treePointer deleteq()
{
	if (front == rear) {
		return NULL;
		//return queueEmpty();	/* return an error key */
	}
	front = (front + 1) % MAX_QUEUE_SIZE;
	return queue[front];
}

/**
 * when push element if stack is full
 * print error and exit program
 */
void queueFull()
{
	fprintf(stderr, "Queue is full, cannot add element \n");
	exit(EXIT_FAILURE);
}

/**
* when pop element if stack is empty
* print error and exit program
* @return	: NULL
*/
treePointer queueEmpty()
{
	fprintf(stderr, "Queue is empty, cannot remove element \n");
	exit(EXIT_FAILURE);
	return NULL;
}

/**
* get the next token, symbol is the character
* representation, which is returned, the token is
* represented by its enumerated value, which
* is returned in the function name
*/
precedence getToken(char *symbol, int *n)
{
	*symbol = expr[(*n)++];

	switch (*symbol) {
	case '(': return lparen;
	case ')': return rparen;
	case '+': return plus;
	case '-': return minus;
	case '/': return divide;
	case '*': return times;
	case '%': return mod;
	case '\0': return eos;
	default: return operand;	/*	no error checking,
								default is operand */
	}
}